<?php
use LeoLab\marray;
use PHPUnit\Framework\TestCase;

class marrayTest extends TestCase
{
    private $a1 = null;
    private $a2 = null;

    // Datasets for tests
    private $data = [
        'd1' => [
            'k1' => 'v1',
            'k2' => 'v2'
        ],
        'd2' => [
            'k3' => 'v3',
            'k4' => 'v4'
        ],
        'd3' => [
            'k2' => [
                'k2s1' => 'v2s1',
                'k2s2' => 'v2s2'
            ]
        ]
    ];

    public function testCreateEmpty()
    {
        $this->a1 = new LeoLab\marray();
        $this->assertSame(0, count($this->a1));
        $this->assertSame(true, empty($this->a1));
    }

    public function testCreate()
    {
        $this->a2 = new LeoLab\marray($this->data['d1']);
        $this->assertSame(count($this->data['d1']), count($this->a2));
        $this->assertSame(false, empty($this->a2));
    }

    public function testAppend()
    {
        $this->a->append(['0', '1']);
        $this->assertSame(2, count($this->a));
        $this->assertSame('1', $a[count($this->a) - 1]);
    }

}
