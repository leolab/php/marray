# Multy Dinamic Array

## Usage
```php
<?php
$a = new LeoLab\marray([]);

?>
```

## Methods
### merge()
### append()
### replace()
### __construct()
### __clone()
### toArray()
### val()
### offsetSet()
### offsetGet()
### offsetExists()
### offsetUnset()
### count()
### rewind()
### current()
### key()
### next()
### valid()
### __set()
### __get()
### __isset()
### __unset()
### serialize()
### unserialize()
## Exceptions
### EPropertyTypeError
### EPropertyNotFound
### EPropertyTypeError
