<?php
/**
 * Реализация многомерного ArrayAccess
 *
 * @author Eugeny Leonov <eleonov@leolab.info>
 *
 * @version 0.1.2-1 (27/03/2020)
 *
 * @changes:
 *
 * #0.1.2-1 27/03/2020
 *     [fix] Некорректный тип возвращаемого значения:
 *         - $->rewind()
 *         - $->current()
 *         - $->key()
 *
 * #0.1.2 (23/03/2020)
 *     - добавлен метод $->val()
 *
 * #0.1.1 (20/03/2020)
 *     - включен в SWC-7
 *
 * #0.1.0.b (09/03/2020)
 *     - перевод класса в бета-версию
 *
 * #0.1.0.a-1 (07/03/2020)
 *     - внедрен метод merge()
 *     - внедрен метод append()
 *
 * #0.1.0 (06/03/2020)
 *     - реализация:
 *         - поддержка многомерных массивов
 *         - поддержка информации об изменении данных
 */

namespace LeoLab;

class marray implements \ArrayAccess, \Countable, \Iterator, \Serializable
{

    protected $_data  = [];
    private $_parent  = null;
    private $_changed = false;

    /**
     * Добавить предоставленный массив с заменой ключей
     *
     * @param array $arr
     * @return void
     *
     * @version 0.1.0
     */
    public function merge(array $arr)
    {
        foreach ($arr as $k => $v) {
            if (!isset($this->_data[$k])) {$this[$k] = $v;} elseif (is_array($v) && ($this->_data[$k] instanceof self)) {$this->_data[$k]->merge($v);} else { $this[$k] = $v;}
        }
        $this->changed = true;
    }

    /**
     * Добавить предоставленный массив без замены ключей
     * Метод рекурсивный
     *
     * @param array $arr
     * @return void
     *
     * @version 0.1.0
     */
    public function append(array $arr)
    {
        foreach ($arr as $k => $v) {
            if (isset($this->_data[$k])) {
                if (($this->_data[$k] instanceof self) && (is_array($v))) {
                    $this->_data[$k]->append($v);
                }
                continue;
            } else { $this[$k] = $v;}
        }
        $this->changed = true;
    }

    /**
     * Заменить данные
     *
     * @param array $arr
     * @return void
     */
    public function replace(array $arr)
    {
        $this->_data = [];
        foreach ($arr as $k => $v) {$his[$k] = $v;}
        $this->_changed = true;
    }

    /**
     * Конструктор класса
     *
     * @param array $data
     * @param \LeoLab\marray $parent
     */
    public function __construct(array $data = [], \LeoLab\marray $parent = null)
    {
        foreach ($data as $k => $v) {$this[$k] = $v;}
        $this->_parent = $parent;
    }

    public function __clone()
    {
        foreach ($this->_data as $k => $v) {if ($v instanceof self) {$this[$k] = clone $v;}}
    }

    /**
     * Возвращает значение как массив
     *
     * @version 0.1.0 (06/03/2020)
     *
     * @return array
     */
    public function toArray()
    {
        $d = $this->_data;
        foreach ($d as $k => $v) {if ($v instanceof self) {$d[$k] = $v->toArray();}}
        return ($d);
    }

    /**
     * Возвращает значение
     *
     * @version 0.1.0 (23/03/2020)
     *
     * @return mixed
     */
    public function val()
    {
        if ($this->_data instanceof self) {return ($this->_data->toArray());}
    }

    public function offsetSet($k, $v)
    {
        if (is_array($v)) {$v = new self($v, $this);}
        if (is_null($k)) {
            $this->_data[] = $v;
        } else {
            $this->_data[$k] = $v;
        }
        $this->changed = true;
    }

    public function &offsetGet($k)
    {
        if (!isset($this->_data[$k])) {$this->_data[$k] = new self([], $this);}
        return ($this->_data[$k]);
    }

    public function offsetExists($k)
    {
        return (isset($this->_data[$k]));
    }

    /**
     * Удалить элемент
     *
     * @param [type] $k
     * @return void
     */
    public function offsetUnset($k)
    {
        if (isset($this->_data[$k])) {unset($this->_data[$k]);}
        $this->changed = true;
    }

    /**
     * Вернуть кол-во элементов
     *
     * @version 0.1.0 (06/03/2020)
     *
     * @return void
     */
    public function count()
    {
        return (count($this->_data));
    }

    /**
     * Установить указатель на первый элемент
     *
     * @version 0.1.0-1 (27/03/2020)
     * #0.1.0-1 (27/03/2020)
     *     [f] Неверный тип возвращаемого значения
     *
     * #0.1.0 (06/03/2020)
     *     [r]
     * @return mixed
     */
    public function rewind()
    {
        return (reset($this->_data));
    }

    /**
     * Получить текущий элемент массива
     * @version 0.1.0-1 (27/03/2020)
     * #0.1.0-1 (27/03/2020)
     *     [f] Неверный тип возвращаемого значения
     *
     * #0.1.0 (06/03/2020)
     *     [r]
     *
     * @return mixed
     */
    public function current()
    {
        return (current($this->_data));
    }

    /**
     * Вернуть текущий ключ
     * @version 0.1.0-1 (27/03/2020)
     * #0.1.0-1 (27/03/2020)
     *     [f] Неверный тип возвращаемого значения
     *
     * #0.1.0 (06/03/2020)
     *     [r]
     * @return mixed
     */
    public function key()
    {
        return (key($this->_data));
    }

    /**
     * Передвинуть указатель вперед и вернуть значение
     *
     * @version 0.1.0 (06/03/2020)
     *
     * @return mixed
     */
    public function next(): mixed
    {
        return (next($this->_data));
    }

    /**
     * Корректна ли текущая позиция
     *
     * @version 0.1.0 (06/03/2020)
     *
     * @return bool
     */
    public function valid(): bool
    {
        return (key($this->_data) !== null);
    }

    public function __set($k, $v)
    {
        switch ($k) {
            case 'changed':
                if (!\is_bool($v)) {throw new EPropertyTypeError($this, $k, $v, 'bool');}
                if (!\is_null($this->_parent)) {$this->_parent->changed = $v;} else { $this->_changed = $v;}
                break;
            default:
                throw new EPropertyNotFound($this, $k);
                break;
        }
    }

    public function __get($k)
    {
        switch ($k) {
            case 'changed':
                if (\is_null($this->_parent)) {return ($this->_parent->changed);} else {return ($this->_changed);}
                break;
            default:
                throw new EPropertyNotFound($this, $k);
                break;
        }
    }

    public function __isset($k)
    {
        switch ($k) {
            case 'changed':return (true);
                break;
            default:return (false);
                break;
        }
    }

    public function __unset($k)
    {
        switch ($k) {
            case 'changed':throw new EPropertyReadOnly($this, $k);
                break;
            default:throw new EPropertyNotFound($this, $k);
                break;
        }
    }

    public function serialize()
    {
        return (serialize($this->toArray()));
    }

    public function unserialize($data)
    {
        $d = unserialize($data);
        foreach ($d as $k => $v) {$this[$k] = $v;}
    }

}

class EPropertyNotFound extends \Exception
{}

class EPropertyReadOnly extends \Exception
{}

class EPropertyTypeError extends \Exception
{}
